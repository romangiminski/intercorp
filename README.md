Możliwa konfiguracja dla supervisord:

```
[program:intercorp-default]
process_name=%(program_name)s_%(process_num)02d
command=php /path/to/project/artisan queue:work redis --queue="default" --tries=1 --timeout=30
autostart=true
autorestart=true
user=[user]
group=[group]
numprocs=4
redirect_stderr=true
stdout_logfile=/path/to/project/storage/logs/queue-default.log
stdout_logfile_maxbytes=2MB
stdout_logfile_backups=4 
```

Dla metody POST przyjąłem taką strukturę JSON:

```
{
	"stats": true,
    "data": [
        "https://onet.pl/",
        "http://socialmention.com/",
        "http://test-redirects.137.software",
        "https://google.com",
        "http://ino-online.pl"
    ]
}
```

Odpowiedź po zarejestrowaniu url-i (POST):

```
{
    "data": {
        "https://onet.pl": {
            "total_time": 0.903473,
            "redirs": 1
        },
        "http://test-redirects.137.software": {
            "total_time": 0.682232,
            "redirs": 3
        },
        "https://google.com": {
            "total_time": 0.38715299999999997,
            "redirs": 1
        },
        "https://ino.online": {
            "total_time": 0.655527,
            "redirs": 0
        },
        "http://ino-online.pl": {
            "total_time": 0.941247,
            "redirs": 1
        }
    },
    "messages": [
        "5 urls added"
    ],
    "code": 200
}
```

Odpowiedź po żądaniu statystyk dla jednego url (GET):

```
{
    "data": [
        {
            "https://onet.pl": [
                {
                    "total_time": 0.903473,
                    "redirs": 1
                },
                {
                    "total_time": 0.995662,
                    "redirs": 1
                },
                {
                    "total_time": 0.809681,
                    "redirs": 1
                },
                {
                    "total_time": 0.823921,
                    "redirs": 1
                }
            ]
        }
    ],
    "messages": null,
    "code": 200
}
```

Routing:

**Routing domyślnie ma prefix 'api', można to zmienić w App\Providers\RouteServiceProvider.php**

```
+--------+-----------+------------------------+------------------+--------------------------------------------+------------+
| Domain | Method    | URI                    | Name             | Action                                     | Middleware |
+--------+-----------+------------------------+------------------+--------------------------------------------+------------+
|        | GET|HEAD  | /                      |                  | Closure                                    | web        |
|        | GET|HEAD  | api/monitors           | monitors.index   | App\Http\Controllers\UrlController@index   | api        |
|        | POST      | api/monitors           | monitors.store   | App\Http\Controllers\UrlController@store   | api        |
|        | GET|HEAD  | api/monitors/{monitor} | monitors.show    | App\Http\Controllers\UrlController@show    | api        |
|        | PUT|PATCH | api/monitors/{monitor} | monitors.update  | App\Http\Controllers\UrlController@update  | api        |
|        | DELETE    | api/monitors/{monitor} | monitors.destroy | App\Http\Controllers\UrlController@destroy | api        |
+--------+-----------+------------------------+------------------+--------------------------------------------+------------+
```


