<?php

return [
    'browser_timeout' => env('INTERCORP_BROWSER_TIMEOUT', 2), //seconds
    'timeout' => env('INTERCORP_TIMEOUT', 30), //seconds
    'period' => env('INTERCORP_PERIOD', 600), //seconds
];
