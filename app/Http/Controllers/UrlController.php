<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UrlController extends Controller
{
    public function index()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return \Intercorp::storeUrlsFromRequest($request)
            ->set('stats_request', $request->stats)
            ->getLiveStats($request)
            ->sendResponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  array/string  $monitor
     * @return \Illuminate\Http\Response
     */
    public function show($monitor)
    {
        return \Intercorp::getStatsFromUrl($monitor)
            ->sendResponse();
    }

    public function update(Request $request, $id)
    {
        return abort(404);
    }

    public function destroy(Request $request, $id)
    {
        return abort(404);
    }
}
