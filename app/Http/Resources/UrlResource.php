<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UrlResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $response[$this->url] = StatResource::collection($this->period);

        if ($response[$this->url]->isEmpty()) {
            $response[$this->url] = [];
            $response[$this->url][] = [
                'total_time' => null,
                'redirs' => null,
            ];
        }

        if ($this->last_status == 0) {
            $response['error'] = [$this->last_error];
        }

        return $response;
    }
}
