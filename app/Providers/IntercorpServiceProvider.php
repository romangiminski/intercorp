<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Intercorp;

class IntercorpServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Intercorp', function () {
            return new Intercorp;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Resource::withoutWrapping();
    }
}
