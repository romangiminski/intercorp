<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class IntercorpFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Intercorp';
    }
}
