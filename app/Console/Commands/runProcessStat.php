<?php

namespace App\Console\Commands;

use App\Jobs\ProcessStat;
use App\Models\Url;
use Illuminate\Console\Command;

class runProcessStat extends Command
{
    protected $urls;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runProcessStat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get stats every minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Url $url)
    {
        parent::__construct();
        $this->urls = $url::all();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach ($this->urls as $url) {
            ProcessStat::dispatch($url);
        }
    }
}
