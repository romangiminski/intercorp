<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Url
 *
 * @property int $id
 * @property string $url
 * @property string $protocol
 * @property string $domain
 * @property string|null $last_error
 * @property int $last_status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Stat[] $last
 * @property-read int|null $last_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Stat[] $period
 * @property-read int|null $period_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Stat[] $stats
 * @property-read int|null $stats_count
 */
class Url extends Model
{
    protected $fillable = ['url', 'protocol', 'domain', 'last_error', 'last_status'];

    public function stats()
    {
        return $this->hasMany(Stat::class);
    }

    public function period()
    {
        return $this->hasMany(Stat::class)->where('updated_at', '>=', now()->subSeconds(config('intercorp.period'))->toDateTimeString('second'));
    }
}
