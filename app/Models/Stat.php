<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Stat
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $url_id
 * @property float|null $total_time
 * @property int|null $redirs
 * @property-read \App\Models\Url $url
 */
class Stat extends Model
{
    protected $fillable = ['total_time', 'redirs'];

    public function url()
    {
        return $this->belongsTo(Url::class);
    }
}
