<?php

namespace App\Library;

use App\Http\Resources\UrlResource;
use App\Jobs\ProcessStat;
use App\Models\Url;
use App\Models\Stat;
use Illuminate\Support\Facades\Redis;
use Symfony\Component\Process\Process;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use Illuminate\Http\Request;

class Intercorp
{
    /**
     * Timeout for CURL operation (seconds).
     * default 30
     * 
     * @var integer
     */
    protected $timeout;

    /**
     * Time od end of script execution.
     *
     * @var Carbon datetime object
     */
    protected $time_end;
    protected $time_start;

    /**
     * Store function timeout (µseconds).
     * default 2 * 1000000
     *
     * @var integer
     */
    protected $browser_timeout_mcs;
    protected $browser_timeout;

    /**
     * Time scope for getting stats for single URL (seconds).
     * default 600
     * 
     * @var integer
     */
    protected $period;

    /**
     * Request for stats.
     * default false
     * 
     * @var boolean
     */
    public $stats_request;

    public $urls_to_store;
    protected $result;

    public function __construct()
    {
        $this->timeout = config('intercorp.timeout');
        $this->period = config('intercorp.period');
        $this->browser_timeout = config('intercorp.browser_timeout');
        $this->browser_timeout_mcs = config('intercorp.browser_timeout') * 1000000; //convert to microseconds
        $this->time_end = now()->addRealMicroseconds($this->browser_timeout_mcs);
        $this->stats_request = 0;
    }

    public function get($property)
    {
        return $this->{$property};
    }

    public function set($property, $value)
    {
        $this->{$property} = $value;
        return $this;
    }

    /**
     * Statystyka dla podanego URL.
     * Korzysta z Guzzle Http.
     *
     * @param  
     *  string $url_to_stats - docelowy URL
     * 
     * @return object $response
     */
    public function getUrlInfoGuzzle($url_to_stats)
    {
        $step = 0;
        $total_time = 0;
        $client = new Client([
            'cookie' => true,
            'allow_redirects' => true,
            'timeout' => $this->stats_request ? $this->browser_timeout : $this->timeout,
            'connection_timeout' => $this->stats_request ? $this->browser_timeout : $this->timeout,
            'return_transfer' => true,
        ]);

        try {
            $client->get($url_to_stats, ['on_stats' => function (TransferStats $stats) use (&$total_time, &$step) {
                if ($stats->hasResponse()) {
                    $total_time += $stats->getTransferTime();
                    if ($stats->getResponse()->getStatusCode() == 301 || $stats->getResponse()->getStatusCode() == 302) {
                        $step++;
                    }
                }
            }]);
        } catch (GuzzleException $e) {
            $stats['error'] = $e->getMessage();
            \Log::error(__CLASS__ . ':' . __FUNCTION__ . ': ' . $e->getMessage());
        }
        $stats['total_time'] = $total_time;
        $stats['redirs'] = $step;
        return (object) $stats;
    }

    /**
     * Store urls from request to database.
     * 
     * Do zapisu dostarczonych adresów uruchamia zewnętrzny proces w tle,
     * w którym wszystkie dostarczone adresy są dopisane do bazy lub z niej pobrane,
     * i dla każdego z nich do kolejki wysłane jest zadanie pobrania statystyk.
     * 
     * Ten sam efekt można uzyskać ustawiając odpowieni Observer dla modelu Url i dla
     * metody save() wykonywać podobną operację. Różnica w czasie dla obu wariantów, to 
     * prawdopodbnie milisekundy w zależności od ilości dostarczanych adresów. Jeżeli jest
     * ich mało, to lepszy będzie wariant z observerem, dla dużych ilości, z zadaniem w tle.
     *
     * @parameter Illuminate\Http\Request request
     * 
     * @return object
     */
    public function storeUrlsFromRequest(Request $request)
    {
        if (!$request->isJson()) {
            $this->result['error'][] = "Niewłaściwy typ danych: oczekiwana struktura JSON";
            $this->result['code'] = 500;
        } elseif (!$request->has('data')  || empty($request->data)) {
            $this->result['error'][] = "Niewłaściwy typ danych: brak parametru 'data' lub jest pusty";
            $this->result['code'] = 500;
        } else {
            $this->urls_to_store = $this->prepareData($request->data);
            if (!empty($this->urls_to_store)) {
                $this->result['messages'][] = 'Dostarczonych adresów: ' . sizeof($this->urls_to_store);
                $key = md5(random_bytes(16));
                Redis::set($key, json_encode($this->urls_to_store));
                $process = new Process("php artisan runProcessUrlFromCache $key > /dev/null 2>&1 &", base_path());
                $process->mustRun();
            } else {
                $this->result['error'][] = "Niewłaściwy typ danych";
                $this->result['code'] = 500;
            }
        }
        return $this;
    }

    /**
     * Get live stats for current request.
     *
     * @parameter Illuminate\Http\Request request
     * 
     * @return object
     */
    public function getLiveStats()
    {
        if ($this->stats_request) {
            while (now()->diffInRealMicroseconds($this->time_end) <= $this->browser_timeout_mcs) {
                if (!empty($this->urls_to_store)) {
                    $url = array_shift($this->urls_to_store);
                    $this->result['data'][$url] = $this->getUrlInfoGuzzle($url);
                }
            }
            $this->result['messages'][] = 'Sprawdzonych adresów: ' . count($this->result['data']);
        }
        return $this;
    }

    /**
     * Store single url to database.
     * 
     * Po zapisaniu do bazy lub pobraniu adresu z bazy, wysyła zadanie
     * pobrania statystyk dla adresu do kolejki. Można z tego zrezygnować
     * i po prostu czekać do kolejnego cyklicznego uruchomienia 
     * zaplanowanego zadania runProcessStat.
     *
     * @parameter string $url - url
     * 
     * @return array
     */
    public function storeSingleUrl($url)
    {
        $attrs = $this->prepareAttrs($url);
        \DB::beginTransaction();
        try {
            $added = Url::firstOrCreate($attrs);
            \DB::commit();
            ProcessStat::dispatch($added); // pobranie statystyk dla adresu do kolejki
        } catch (\Exception $e) {
            \Log::error(__CLASS__ . ':' . __FUNCTION__ . ': ' . $e->getMessage());
            \DB::rollback();
        } catch (\PDOException $e) {
            \Log::error(__CLASS__ . ':' . __FUNCTION__ . ': ' . $e->getMessage());
            \DB::rollback();
        }
        return $added;
    }

    /**
     * Store stats for single url to database.
     *
     * @parameter App\Models\Url $url
     * @parameter array $stats
     * 
     * @return array
     */
    public function storeStatsForUrl(Url $url, $stats = null)
    {
        $stats = $stats ?? $this->getUrlInfoGuzzle($url->url);
        if (!$this->storeStatsDataToDB($url, $stats)) {
            \Log::error(__CLASS__ . ':' . __FUNCTION__ . ": błąd zapisu statystyk w DB: " . $url . ":" . json_encode($stats));
        }
    }

    /**
     * Store stats urls from redis cache to database.
     *
     * @parameter string $cache_key
     * 
     * @return array
     */
    public function storeStatsFromCache($cache_key)
    {
        $data = json_decode(Redis::get($cache_key));
        if ($data && is_array($data)) {
            foreach ($data as $url_to_add) {
                $this->storeSingleUrl($url_to_add);
            }
        } else {
            $this->result['messages'][] = 'Brak danych w cache lub zły format danych';
            $this->result['code'] = 204;
            \Log::info(__CLASS__ . ':' . __FUNCTION__ . ': brak danych w cache lub zły format danych');
        }
        Redis::del($cache_key);
    }

    /**
     * Get stats from db for single url.
     *
     * @parameter $monitor - url from GET query
     * 
     * if scheme not present domain is searched for both, possible two results
     * 
     * @return array
     */
    public function getStatsFromUrl($monitor)
    {
        $domain = strtolower(parse_url($monitor, PHP_URL_HOST));
        $protocol = strtolower(parse_url($monitor, PHP_URL_SCHEME));
        if ($domain && ($protocol == 'http' || $protocol == 'https')) {
            $this->result['data'] = new UrlResource(Url::with('period')->where(['protocol' => $protocol, 'domain' => $domain])->first());
        } elseif ($domain) {
            $this->result['data'] = UrlResource::collection(Url::with('period')->where('domain', $domain)->get());
        } else {
            $this->result['data'] = UrlResource::collection(Url::with('period')->where('url', 'like', '%' . $monitor . '%')->get());
        }
        if ($this->result['data']->isEmpty()) {
            $this->result['messages'][] = 'Brak danych do wysłania';
            $this->result['code'] = 204;
        }
        return $this;
    }

    public function sendResponse()
    {
        if (array_key_exists('error', $this->result) && $this->result['error']) {
            return $this->sendError();
        } else {
            return $this->sendSuccess();
        }
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    private function sendSuccess()
    {
        $response = [
            'data'    => $this->result['data'] ?? null,
            'messages' => $this->result['messages'] ?? null,
            'code' => $this->result['code'] ?? 200,
        ];
        $res = response()->json($response, $response['code']);
        if ($this->stats_request) {
            $res = $res->header('X-Stat', json_encode(json_decode($res->getContent())->data));
        }

        return $res;
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    private function sendError($errorMessages = [], $code = 404, $result = null)
    {
        $response = [
            'code' => $this->result['code'] ?? 500,
            'data' => $this->result['data'] ?? null,
            'errors' => $this->result['error'],
        ];
        dump($response);
        return response()->json($response, $this->result['code']);
    }

    /**
     * Prepare attributes to store.
     *
     * @return array
     */
    private function prepareAttrs($url)
    {
        $parts = parse_url($url);
        if ($parts['scheme'] == 'http' || $parts['scheme'] == 'https') {
            $attrs['url'] = $url;
            $attrs['protocol'] = $parts['scheme'];
            $attrs['domain'] = $parts['host'];
        } else {
            $attrs = null;
            \Log::error(__CLASS__ . ':' . __FUNCTION__ . ': malformed url: ' . $url);
        }
        return $attrs;
    }

    private function prepareData($data)
    {
        foreach ($data as $url) {
            $urls[] = rtrim(filter_var($url, FILTER_SANITIZE_URL), '/');
        }
        return $urls;
    }

    private function storeStatsDataToDB(Url $url, $stats)
    {
        $result = false;
        if ($url && $stats) {
            if (!property_exists($stats, 'error') || is_null($stats->error)) {
                \DB::beginTransaction();
                try {
                    $stat = new Stat();
                    $stat->total_time = $stats->total_time;
                    $stat->redirs = $stats->redirs;
                    $url->stats()->save($stat);
                    $url->update(['last_status' => true, 'last_error' => null]);
                    \DB::commit();
                    $result = true;
                } catch (\Exception $e) {
                    \Log::error(__CLASS__ . ':' . __FUNCTION__ . ': ' . $e->getMessage());
                    \DB::rollback();
                } catch (\PDOException $e) {
                    \Log::error(__CLASS__ . ':' . __FUNCTION__ . ': ' . $e->getMessage());
                    \DB::rollback();
                }
            } else {
                $url->update(['last_error' => $stats->error ?? 'Nieznany błąd', 'last_status' => false]);
                $result = true;
            }
        }

        return $result;
    }
}
